package practicalspackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
public class Sortingemployeerecords {
	

	public static void main(String[] args) {
		/*create Employee objects*/
		Employee e1 = new Employee("1","sai","Mangalagiri");
		Employee e2 = new Employee("4","anil","vijayawada");
		Employee e3 = new Employee("8","satya","Hyderabad");
		Employee e4 = new Employee("3","vamsi","Guntur");
		Employee e5 = new Employee("5","hari","Tenali");
		Employee e6 = new Employee("7","priya","Anakapali");
		Employee e7 = new Employee("2","pavan","Vizag");
		Employee e8 = new Employee("6","pavani","Ananthapur");
		Employee e9 = new Employee("9","jagan","Karimnagar");
		Employee e10 = new Employee("10","chandra","chennai");
		/*create a list of employees*/
		List<Employee> list = new ArrayList<Employee>();

		/*add employees to the list*/
		list.add(e1);
		list.add(e2);
		list.add(e3);
		list.add(e4);
		list.add(e5);
		list.add(e6);
		list.add(e7);
		list.add(e8);
		list.add(e9);
		list.add(e10);
		
		System.out.println("Sorting Employee Records");

		System.out.println("Before Sort");
		/*get the iterator object from list */
		Iterator iterator1 = list.iterator();

		while (iterator1.hasNext()) {

			/* get the employee*/
			Employee employee = (Employee) iterator1.next();
		}
		System.out.println(String.format("%-10s%-10s%-10s", "ID", "Name", "Adress"));
		for(Employee str: list)
    	/*print the employee Id*/
		System.out.println(""+str);
		/*Below method sorts the Employee object*/
		Collections.sort(list);
		
		System.out.println("After Sort");

		/*get the iterator object from list */
		Iterator iterator2 = list.iterator();

		while (iterator2.hasNext()) {
			/*get the employee*/
			Employee employee = (Employee) iterator2.next();

		}
		System.out.println(String.format("%-10s%-10s%-10s", "ID", "Name", "Adress"));
		for(Employee str2: list)
			System.out.println(""+str2);
			

	}
}
