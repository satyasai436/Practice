package practicalspackage;

/*We must implement Comparable interface and override compareTo method*/
public class Employee implements Comparable<Employee>
{
	 String empId,empName,address;
	public  Employee(String empId, String empName, String address) {
		this.empId = empId;
		this.empName=empName;
		this.address=address;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getaddress() {
		return address;
	}
	public void setaddress(String address) {
		this.address = address;
	}
	/* compareTo method is used while sorting an employee */
	public int compareTo(Employee o) {
		/*we compare two names of the employee to figure out,two employees are equal or not*/
		return this.empId.compareTo(o.getEmpId());
	}
	public String toString() {
        return this.empId+"	"+this.empName+"	"+this.address;
    }
}
