package abc;

public class Assignment1Strings {
	public static void main(String args[]) {
		//Storing String value
		String s = "asdfadsfds admin@gamil.com asdfsadfasdf user@gmail.com sdsdfadasfasdf";
		//Storing string value in buffer
		StringBuffer s1 = new StringBuffer(s);
		//Printing Reverse string using StringBuffer
		System.out.println("The Reverse string value for given string using StringBuffer: "+s1.reverse());
		
		//Reverse string concept using for loop
		 String res="";
		 int length = s.length();
		 for ( int i = length-1; i >= 0 ; i-- )
		 {
	         res = res+ s.charAt(i);
	         
		 }
		 //Printing Reverse string value using For loop
		 System.out.println("The Reverse string value for given string using Forloop: "+res);
		 
		 //extract list of email ids from the given string
		//System.out.println("Extracted dummy string1: "+s.substring(0, 10));// extract asdfadsfds
		System.out.println("Extracted Email1: "+s.substring(11,26 ));// extract admin@gamil.com
		//System.out.println("Extracted dummy string2: "+s.substring(27,39));//extract asdfsadfasdf
		System.out.println("Extracted Email2: "+s.substring(40,54));//extract user@gmail.com
		//System.out.println("Extracted dummy string3: "+s.substring(56,69));//extract sdsdfadasfasdf
	}
}
