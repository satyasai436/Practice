package abc;

import org.junit.*;

public class JUnitAnnotations {
	@Before
	public void setUp()
	{
		System.out.println("@Before");
	}
	@After
	public void tearDown()
	{
		System.out.println("@After");
	} 
	@BeforeClass
	public static void oneTimeSetUP()
	{
		System.out.println("@BeforeClass");
	}
	@AfterClass
	public static void oneTmeTearDown()
	{
		System.out.println("@AfterClass");
	} 
	@Test
	public void testA()
	{
		System.out.println("@Test1");
	}
	@Test
	public void testB()
	{
		System.out.println("@Test2");
	}

}
